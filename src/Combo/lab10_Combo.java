package Combo;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class lab10_Combo {
	public lab10_Combo(){
		createGUI();
	}
	public void createGUI(){
		JFrame Frame = new JFrame("Lab10_GUI");
		Frame.setSize(700, 500);
		Frame.setLayout(new BorderLayout());
		Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		Frame.add(panel,BorderLayout.SOUTH);
		
		JComboBox Combo = new JComboBox();
		Combo.addItem("Red");
		Combo.addItem("Green");
		Combo.addItem("Blue");
		Combo.setLayout(new BorderLayout());
		panel.add(Combo);
		
		Combo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String Item = String.valueOf(Combo.getSelectedItem());
				if (Item=="Red"){
					Frame.getContentPane().setBackground(Color.RED);
					panel.setBackground(Color.RED);
				}
				else if (Item=="Green"){
					Frame.getContentPane().setBackground(Color.GREEN);
					panel.setBackground(Color.GREEN);
				}
				else if (Item=="Blue"){
					Frame.getContentPane().setBackground(Color.BLUE);
					panel.setBackground(Color.BLUE);
				}
				
			}
		});
		
		
		Frame.setVisible(true);
		Frame.setResizable(false);
	}
	}



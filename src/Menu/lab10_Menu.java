package Menu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class lab10_Menu {
	public lab10_Menu(){
		createGUI();
	}
	public void createGUI(){
		JFrame Frame = new JFrame("Lab10_GUI");
		Frame.setSize(700, 500);
		Frame.setLayout(new BorderLayout());
		Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		Frame.add(panel,BorderLayout.SOUTH);
		
		JMenuBar menu = new JMenuBar();
		JMenu red = new JMenu("Red");
		JMenu green = new JMenu("Green");
		JMenu blue = new JMenu("Blue");
		menu.add(red);
		menu.add(green);
		menu.add(blue);
		panel.add(menu);
		
		
		red.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				Frame.getContentPane().setBackground(Color.RED);
				panel.setBackground(Color.RED);
				
			}
		});
		
		green.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				Frame.getContentPane().setBackground(Color.GREEN);
				panel.setBackground(Color.GREEN);
				
			}
		});
		
		blue.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				Frame.getContentPane().setBackground(Color.BLUE);
				panel.setBackground(Color.BLUE);
				
			}
		});
		
		

		
		
		
		
		Frame.setVisible(true);
		Frame.setResizable(false);
	}
}




package Button;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;



public class lab10_Button {
	public lab10_Button(){
		createGUI();
	}
	public void createGUI(){
		JFrame Frame = new JFrame("Lab10_GUI");
		Frame.setSize(700, 500);
		Frame.setLayout(new BorderLayout());
		Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		Frame.add(panel,BorderLayout.SOUTH);
		
		
		JButton B1 = new JButton("Red");
		B1.setLayout(new BorderLayout());
		panel.add(B1);
		
		JButton B2 = new JButton("Green");
		B2.setLayout(new BorderLayout());
		panel.add(B2);
		
		JButton B3 = new JButton("Blue");
		B3.setLayout(new BorderLayout());
		panel.add(B3);
		
		B1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Frame.getContentPane().setBackground(Color.RED);
				panel.setBackground(Color.RED);
				
			}
		});
		
		B2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Frame.getContentPane().setBackground(Color.GREEN);
				panel.setBackground(Color.GREEN);
				
			}
		});
		
		B3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Frame.getContentPane().setBackground(Color.BLUE);
				panel.setBackground(Color.BLUE);
				
			}
		});
		
		
		
		
		
		Frame.setVisible(true);
		Frame.setResizable(false);
	}

}

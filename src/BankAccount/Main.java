package BankAccount;
import javax.swing.JFrame;


 
public class Main {
	
	public BankAccountFrame View;
	private BankAccount account;
	private static final double INITIAL_BALANCE = 100;
	private static final double DEFAULT_MONEY = 5;
	private static final int FRAME_WIDTH = 500;
	private static final int FRAME_HEIGHT = 100;

	public static void main(String[] args) {
		new Main();

	}
	public Main(){
		account = new BankAccount(INITIAL_BALANCE);
		View = new BankAccountFrame(account);
		View.createTextField(DEFAULT_MONEY);
		View.createButton(account);
		View.createPanel();
		View.setVisible(true);
		View.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    View.setSize(FRAME_WIDTH, FRAME_HEIGHT);
	}

}

package BankAccount;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BankAccountFrame extends JFrame {
	   private static final int FRAME_WIDTH = 500;
	   private static final int FRAME_HEIGHT = 100;
	   
	   private JLabel MoneyLabel;
	   private JTextField MoneyField;
	   private JButton button;
	   private JButton withdraw;
	   private JLabel resultLabel;
	   private JPanel panel;
	  
	   public BankAccountFrame(BankAccount b) { 
	     resultLabel = new JLabel("balance: " + b.getBalance());
	   }
	   public void createTextField(double x){
	      MoneyLabel = new JLabel("Money: ");

	      final int FIELD_WIDTH = 10;
	      MoneyField = new JTextField(FIELD_WIDTH);
	      MoneyField.setText("" + x);
	   }
	   
	   public void createButton(BankAccount ac){
	      button = new JButton("Deposite");
	      
	      class AddDepositeListener implements ActionListener{
	         public void actionPerformed(ActionEvent event){
	            double rate = Double.parseDouble(MoneyField.getText());
	            ac.deposit(rate);
	            resultLabel.setText("Balance: " + ac.getBalance());
	         }            
	      }
	      
	      ActionListener listener = new AddDepositeListener();
	      button.addActionListener(listener);
	   }
	   
	   public void createWithdraw(BankAccount ac){
		   withdraw = new JButton("WithDraw");
		   
		   class AddWithdrawListerner implements ActionListener{
			   public void actionPerformed(ActionEvent e){
				   double rate = Double.parseDouble(MoneyField.getText());
				   ac.withdraw(rate);
				   resultLabel.setText("Balance: " + ac.getBalance());
			   }
		   }
		   ActionListener listener = new AddWithdrawListerner();
		   withdraw.addActionListener(listener);
	   }


	   public void createPanel(){
	      panel = new JPanel();
	      panel.add(MoneyLabel);
	      panel.add(MoneyField);
	      panel.add(button);
	      //panel.add(withdraw);
	      panel.add(resultLabel);      
	      add(panel);
	   } 
	   

}
